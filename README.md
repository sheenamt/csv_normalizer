# Tool that reads a CSV formatted file from stdin and emits a UTF-8 normalized CSV formatted file on stdout. 

## Dependencies:
This script requires pytz and python3

## Example setup:

python3 -m venv normalizer-env  
source normalizer-env/bin/activate  
pip install -r requirements.txt

## Example usage: 

./normalizer.py < sample.csv > output.csv
