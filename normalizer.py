#!/usr/bin/env python

import csv
import sys
import io
from datetime import datetime
from pytz import timezone


def fix_zip(info):
    '''Given the zip code, return a string prefixed
    with zeros of length 5'''
    return "{:05d}".format(int(info))


def convert_timezone(info):
    '''Convert from US/Pacific to US/Eastern'''
    # Read time in as US/Pacific
    pactime = datetime.strptime(info, '%m/%d/%y %I:%M:%S %p').astimezone(tz=timezone('US/Pacific'))
    # Return time as US/Eastern, in RFC3339 format
    return str(pactime.astimezone(tz=timezone('US/Eastern')).isoformat())


def duration(info):
    '''Return duration in seconds'''
    hrs, mins, secs = [float(i) for i in info.split(':')]
    return((hrs * 60 + mins) * 60 + secs)


# The entire CSV is in the UTF-8 character set.
data = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8', errors='replace')
reader = csv.DictReader(data)
header = reader.fieldnames
writer = csv.DictWriter(sys.stdout, fieldnames=header)
writer.writeheader()

for row in reader:
    try:
        # All ZIP codes should be 5 digits,use 0 as prefix
        row['ZIP'] = fix_zip(row['ZIP'])
        # The FullName column should be converted to uppercase
        row['FullName'] = row['FullName'].upper()
        # The FooDuration and BarDuration should be in second as float
        row['FooDuration'] = duration(row['FooDuration'])
        row['BarDuration'] = duration(row['BarDuration'])
        # The TotalDuration column is the sum of FooDuration and BarDuration.
        row['TotalDuration'] = row['FooDuration'] + row['BarDuration']
        # The Timestamp column should be formatted in RFC3339 format.
        # Input assumed to be in US/Pacific time; convert it to US/Eastern.
        row['Timestamp'] = convert_timezone(row['Timestamp'])
        writer.writerow(row)
    except ValueError:
        sys.stderr.write('dropping row due to bad character {} \n'.format(list(row.values())))
